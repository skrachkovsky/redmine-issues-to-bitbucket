## About

`redmine_issues_to_bitbucket.py` is a small script that retrieves issues from [Redmine's REST API](http://www.redmine.org/projects/redmine/wiki/Rest_api) and generates zip files that can be imported into Bitbucket's issue tracker [using the issue importer](https://confluence.atlassian.com/display/BITBUCKET/Export+or+import+issue+data).

## Requirements

* Python 3.4
* Pandoc

## Installation
 
* _Using virtualenv_
* (venv) \> `pip install -r requirements.txt`
* Create `local_settings.py`. You could override variables from `settings.py` there.

## Usage
Edit the `redmine_issues_to_bitbucket.py` file and change the variables at the top to match your
project. Then, run the following:

* (venv) \> `python redmine_issues_to_bitbucket.py <redmineUrl> <redmineApiKey> <projectId>`

Example:

* (venv) \> `python redmine_issues_to_bitbucket.py http://redmine.jci.org f00ba5 test_project`

The "issues" directory will then be populated with one zip file for each project in Redmine. See
the [Bitbucket documentation](https://confluence.atlassian.com/display/BITBUCKET/Export+or+import+issue+data#Exportorimportissuedata-HowtoImport)
for how to import these files.

### Preserving Bitbucket Issues
If you have existing issues in Bitbucket that you'd like to preserve, do an export for each project and
place the zip files in the "issues\_old" directory. The script will automatically merge
those issues with the ones in Redmine. In the case of a conflict, the Redmine issue will overwrite
the Bitbucket one.
