import json
import zipfile
import re
from redmine.exceptions import ResourceBadMethodError, ResourceAttrError
from documents import Converter
from settings import *
import inspect
from hashlib import sha1


class MapException(BaseException):
    pass


class Decorator:
    _cached = {}

    def cached(self, manager, attr=None, id_name=None, id_value=None, filters=None):
        do_refresh = True
        obj_name = manager.__name__ if inspect.isclass(manager) else manager.__class__.__name__
        if filters:
            obj_name += '__' + sha1(str(filters).encode('utf-8')).hexdigest()
        if not self._cached.get(obj_name):
            self._cached[obj_name] = {}
        if id_value:
            obj_id = id_value
            if filters:
                mlist = manager.filter(**filters)
                for m in mlist:
                    self._cached[obj_name][m.id] = m
                    if m.id == id_value:
                        manager = m
                        obj_id = m.id
            elif hasattr(manager, 'get'):
                try:
                    manager = manager.get(id_value)
                    obj_id = manager.id
                except ResourceBadMethodError:
                    if hasattr(manager, 'all'):
                        try:
                            mlist = manager.all()
                            for m in mlist:
                                self._cached[obj_name][m.id] = m
                                if m.id == id_value:
                                    manager = m
                                    obj_id = m.id
                        except ResourceBadMethodError:
                            pass
            do_refresh = False
        else:
            obj_id = getattr(manager, id_name) if id_name else manager.id
        if not obj_id:
            return None
        if obj_id not in self._cached[obj_name]:
            self._cached[obj_name][obj_id] = \
                manager.refresh() if do_refresh else manager
        if attr:
            if self._cached[obj_name][obj_id]:
                return getattr(self._cached[obj_name][obj_id], attr, None)
            return None
        return self._cached[obj_name][obj_id]

    def __init__(self, redmine=None):
        self.redmine = redmine

    def get_user(self, username):
        if username not in USER_MAP:
            print('\tCouldn\'t find username "%s" in USER_MAP' % username)
            if MARK_UNMAPPED_USERS_AS_ANONYMOUS:
                return None
            raise MapException
        return USER_MAP[username]

    def get_status(self, status_name):
        if status_name not in STATUS_MAP:
            print('\tCouldn\'t find status "%s" in STATUS_MAP' % status_name)
            raise MapException
        return STATUS_MAP[status_name]

    def get_tracker(self, tracker_name):
        if tracker_name not in TRACKER_TO_KIND_MAP:
            print('\tCouldn\'t find tracker "%s" in TRACKER_TO_KIND_MAP' % tracker_name)
            raise MapException
        return TRACKER_TO_KIND_MAP[tracker_name]

    def get_priority(self, priority_name):
        if priority_name not in PRIORITY_MAP:
            print('\tCouldn\'t find priority "%s" in PRIORITY_MAP' % priority_name)
            raise MapException
        return PRIORITY_MAP[priority_name]

    def _prepare_link(self, link):
        return re.sub('^https?://.+?/', self.redmine.url + '/', link)

    def _prepare_text(self, text):
        return Converter(TEXT_CONVERSION[0], TEXT_CONVERSION[1], text).result


class RedmineIssueFeed(Decorator):
    def __init__(self, redmine=None):
        super().__init__(redmine)
        self.db_filename = 'db-1.0.json'
        # self.dbs maps repo slugs to dictionaries.
        # Each dictionary will be serialized into a db-1.0.json file that'll
        # be used to generate the ZIP file for the associated repo.
        self.dbs = {}

    def add_issue(self, redmine_issue):
        # Takes in an Isssue object and sets appropritate fields in self.dbs
        print('Processing redmine issue #%i' % redmine_issue.id)

        repo = redmine_issue.project.name

        if repo not in self.dbs:
            self.load_issues_for_repo(repo)

        try:
            author_username = self.get_user(self.cached(redmine_issue.author, 'login'))
            status_name = self.get_status(redmine_issue.status.name)
            tracker_name = self.get_tracker(redmine_issue.tracker.name)
            priority_name = self.get_priority(redmine_issue.priority.name)
        except MapException:
            print('\tSkipping issue')
            return

        try:
            assignee_username = self.get_user(self.cached(redmine_issue.assigned_to, 'login'))
            content = redmine_issue.description
        except (MapException, ResourceAttrError):
            assignee_username = None
            content = ''

        issue = {
            'assignee': assignee_username,
            'component': None,
            'content': content,
            'content_updated_on': redmine_issue.updated_on.isoformat(),
            'created_on': redmine_issue.created_on.isoformat(),
            'edited_on': None,
            'id': redmine_issue.id,
            'kind': tracker_name,
            'milestone': 'M2',
            'priority': priority_name,
            'reporter': author_username,
            'status': status_name,
            'title': redmine_issue.subject,
            'updated_on': redmine_issue.updated_on.isoformat(),
            'version': None,
            'watchers': [],
            'voters': []
        }

        for watcher in redmine_issue.watchers:
            try:
                watcher_username = self.get_user(self.cached(watcher, 'login'))
                if watcher_username and watcher_username not in issue['watchers']:
                    issue['watchers'].append(watcher_username)
            except MapException:
                pass

        if EXPORT_ATTACHMENTS:
            for attachment in redmine_issue.attachments:
                attachment_obj = self.get_issue_attachments(attachment, redmine_issue)
                if attachment_obj:
                    try:
                        at_id = next(i for i, exatt in enumerate(self.dbs[repo]['attachments'])
                                     if exatt['path'] == attachment_obj['path'])
                        self.dbs[repo]['attachments'][at_id] = attachment_obj
                    except StopIteration:
                        self.dbs[repo]['attachments'].append(attachment_obj)

        try:
            version = {'name': redmine_issue.fixed_version.name}
            if version not in self.dbs[repo]['versions']:
                self.dbs[repo]['versions'].append(version)
            issue['version'] = version['name']
        except AttributeError:
            issue['version'] = None

        try:
            component = {'name': redmine_issue.category.name}
            if component not in self.dbs[repo]['components']:
                self.dbs[repo]['components'].append(component)
            issue['component'] = component['name']
        except AttributeError:
            issue['component'] = None

        if SAVE_ADDITIONAL_FIELDS:
            additional_content = self.get_additional_content(redmine_issue)
            if additional_content:
                if not issue['content']:
                    issue['content'] = ''
                issue['content'] += '\n\n' + additional_content

        if TEXT_CONVERSION and issue['content']:
            issue['content'] = self._prepare_text(issue['content'])

        # de-dupe by looping over all existing issues and comparing the 'id' field
        # Very inefficient, but the performance hit isn't that bad, and this is meant
        # to be a one-off script anyway.
        try:
            iss_id = next(i for i, exissue in enumerate(self.dbs[repo]['issues']) if issue['id'] == exissue['id'])
            self.dbs[repo]['issues'][iss_id] = issue
        except StopIteration:
            self.dbs[repo]['issues'].append(issue)

        for journal in redmine_issue.journals:
            comment = self.get_comment(journal, redmine_issue)
            if comment:
                # de-dupe in the same way as above
                try:
                    cm_id = next(i for i, excomm
                                 in enumerate(self.dbs[repo]['comments']) if excomm['id'] == comment['id'])
                    self.dbs[repo]['comments'][cm_id] = comment
                except StopIteration:
                    self.dbs[repo]['comments'].append(comment)
                if ADD_WATCHERS:
                    if comment['user'] and comment['user'] not in issue['watchers']:
                        issue['watchers'].append(comment['user'])
            for detail in journal.details:
                log = self.get_log(detail, redmine_issue, journal)
                if log:
                    self.dbs[repo]['logs'].append(log)
                    if ADD_WATCHERS:
                        if log['field'] == 'assignee':
                            for log_f in ['changed_from', 'changed_to']:
                                if log[log_f] and log[log_f] not in issue['watchers']:
                                    issue['watchers'].append(log[log_f])

        return issue

    def get_issue_attachments(self, attachment, redmine_issue):
        if attachment.filename:
            try:
                path = '%s_%s' % (attachment.id, attachment.filename)
                self.redmine.download(self._prepare_link(attachment.content_url), OUTPUT_DIR, path)
                try:
                    username = self.get_user(self.cached(attachment.author, 'login'))
                except MapException:
                    print('\tSkipping attachment')
                    return None
                return {
                    'filename': attachment.filename,
                    'issue': redmine_issue.id,
                    'path': path,
                    'user': username
                }
            except Exception as exc:
                print('\t' + str(exc))
                print('\tCoundn\'t download file "%s"' % attachment.content_url)

    def load_issues_for_repo(self, repo):
        print('\tAttempting to load exported Bitbucket issues for repo "%s"' % repo),
        try:
            archive = zipfile.ZipFile(os.path.join(INPUT_DIR, '%s-issues.zip' % repo), 'r')
            self.dbs[repo] = json.loads(archive.read(self.db_filename))
            self.dbs[repo]['logs'] = []
            for f in self.dbs[repo]['attachments']:
                archive.extract(f['path'], OUTPUT_DIR)
            print('\t...loaded %s issues' % len(self.dbs[repo]['issues']))
            print('\t   loaded %s attachments' % len(self.dbs[repo]['attachments']))
        except (KeyError, IOError):
            print('\t...none found')
            self.dbs[repo] = {
                'issues': [],
                'comments': [],
                'components': [],
                'milestones': [],
                'versions': [],
                'attachments': [],
                'logs': [],
                'meta': {
                    'default_assignee': None,
                    'default_component': None,
                    'default_kind': 'bug',
                    'default_milestone': None,
                    'default_version': None,
                }
            }

    def get_comment(self, journal, redmine_issue):
        try:
            username = self.get_user(self.cached(journal.user, 'login'))
        except MapException:
            print('\tSkipping comment')
            return None
        try:
            content = journal.notes
        except ResourceAttrError:
            content = ''
        if TEXT_CONVERSION and content:
            content = self._prepare_text(content)
        return {
            'content': content,
            'created_on': journal.created_on.isoformat(),
            'id': journal.id,
            'issue': redmine_issue.id,
            'updated_on': None,
            'user': username
        }

    def get_log(self, detail, redmine_issue, journal):
        try:
            username = self.get_user(self.cached(journal.user, 'login'))
        except MapException:
            print('\tSkipping log')
            return None
        ret = {
            'changed_from': None,
            'changed_to': None,
            'comment': journal.id,
            'created_on': journal.created_on.isoformat(),
            'field': None,
            'issue': redmine_issue.id,
            'user': username
        }
        if detail['name'] == 'status_id':
            ret['field'] = 'status'
            try:
                if detail.get('old_value'):
                    ret['changed_from'] = self.get_status(
                        self.cached(self.redmine.issue_status, 'name', id_value=int(detail['old_value'])))
                ret['changed_to'] = self.get_status(
                    self.cached(self.redmine.issue_status, 'name', id_value=int(detail['new_value'])))
            except (MapException, ValueError):
                print('\tSkipping log')
                return None
        elif detail['name'] == 'assigned_to_id':
            ret['field'] = 'assignee'
            try:
                if detail.get('old_value'):
                    ret['changed_from'] = self.get_user(
                        self.cached(self.redmine.user, 'login', id_value=int(detail['old_value'])))
                ret['changed_to'] = self.get_user(
                    self.cached(self.redmine.user, 'login', id_value=int(detail['new_value'])))
            except (MapException, ValueError):
                print('\tSkipping log')
                return None
        elif detail['name'] == 'tracker_id':
            ret['field'] = 'kind'
            try:
                if detail.get('old_value'):
                    ret['changed_from'] = self.get_tracker(
                        self.cached(self.redmine.tracker, 'name', id_value=int(detail['old_value'])))
                ret['changed_to'] = self.get_tracker(
                    self.cached(self.redmine.tracker, 'name', id_value=int(detail['new_value'])))
            except (MapException, ValueError):
                print('\tSkipping log')
                return None
        elif detail['name'] == 'priority_id':
            ret['field'] = 'priority'
            try:
                if detail.get('old_value'):
                    ret['changed_from'] = self.get_priority(
                        self.cached(self.redmine.enumeration, 'name', id_value=int(detail['old_value']),
                                    filters={'resource': 'issue_priorities'}))
                ret['changed_to'] = self.get_priority(
                    self.cached(self.redmine.enumeration, 'name', id_value=int(detail['new_value']),
                                filters={'resource': 'issue_priorities'}))
            except (MapException, ValueError):
                print('\tSkipping log')
                return None
        elif detail['name'] == 'description':
            ret['field'] = 'content'
            if detail.get('old_value'):
                ret['changed_from'] = detail['old_value']
            ret['changed_to'] = detail['new_value']
            if TEXT_CONVERSION:
                ret['changed_from'] = self._prepare_text(ret['changed_from'])
                ret['changed_to'] = self._prepare_text(ret['changed_to'])
        elif detail['name'] == 'subject':
            ret['field'] = 'title'
            if detail.get('old_value'):
                ret['changed_from'] = detail['old_value']
            ret['changed_to'] = detail['new_value']
            if TEXT_CONVERSION:
                ret['changed_from'] = self._prepare_text(ret['changed_from'])
                ret['changed_to'] = self._prepare_text(ret['changed_to'])
        elif detail['property'] == 'attachment':
            ret['field'] = 'attachment'
            ret['changed_to'] = detail['new_value']
        else:
            print('\tSkipping log')
            print('\t\tNot implemented: %s' % detail)
            return None
        if not ret['changed_to'] or not ret['field']:
            return None
        return ret

    def get_additional_content(self, redmine_issue):
        # Bitbucket doesn't support these fields yet, so append them to issue description
        # as an extra line so they don't get lost
        additional_fields = {
            'Start date': lambda i: i.start_date.strftime('%Y-%m-%d'),
            'Due date': lambda i: i.due_date.strftime('%Y-%m-%d'),
            'Spent hours': lambda i: i.spent_hours,
            'Estimated hours': lambda i: i.estimated_hours,
            '% Done': lambda i: str(i.done_ratio) + "%",
        }
        additional_content = ""
        for (label, value_fetch) in additional_fields.items():
            try:
                value = value_fetch(redmine_issue)
                additional_content += "\n%s: %s\n" % (label, value)
            except AttributeError:
                pass
        return additional_content.strip()

    def save_issues(self):
        for repo in self.dbs:
            self.save_issues_for_repo(repo)

    def save_issues_for_repo(self, repo):
        print('Saving issues for repo %s' % repo)

        archive = zipfile.ZipFile(os.path.join(OUTPUT_DIR, '%s-issues.zip' % repo), 'w')
        db = json.dumps(self.dbs[repo], indent=3)
        archive.writestr(self.db_filename, db)
        for file in self.dbs[repo]['attachments']:
            archive.write(os.path.join(OUTPUT_DIR, file['path']), file['path'])
        archive.close()
        for file in self.dbs[repo]['attachments']:
            os.remove(os.path.join(OUTPUT_DIR, file['path']))
