import os
import subprocess
from uuid import uuid1
from settings import TMP_DIR


class Converter:
    _result_text = None

    def __init__(self, convert_from, convert_to, text):
        self.convert_from = convert_from
        self.convert_to = convert_to
        self.source_text = text
        self.result_name = uuid1().hex

    @property
    def result(self):
        if self._result_text is None:
            with open(os.path.join(TMP_DIR, self.result_name + '_source'), 'wb') as source:
                source.write(self.source_text.encode('utf-8'))
            subprocess.call([
                'pandoc',
                os.path.join(TMP_DIR, self.result_name + '_source'),
                '-f',
                self.convert_from,
                '-t',
                self.convert_to,
                '-o',
                os.path.join(TMP_DIR, self.result_name)
            ], shell=True)
            with open(os.path.join(TMP_DIR, self.result_name), 'rb') as res:
                self._result_text = res.read().decode('utf-8')
            if self._result_text is None:
                self._result_text = ''
        return self._result_text

    def __del__(self):
        for f in [self.result_name, self.result_name + '_source']:
            if os.path.isfile(os.path.join(TMP_DIR, f)):
                os.remove(os.path.join(TMP_DIR, f))
