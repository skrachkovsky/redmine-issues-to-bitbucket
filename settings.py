import os

BASE_DIR = os.path.dirname(__file__)

# Map Redmine usernames to bitbucket usernames
USER_MAP = {
    'redmine_user': 'bitbucket_user',
    None: None,  # this is for the "Anonymous" Redmine user
}
# Map tracker names in Redmine to Bitbucket bug types (i.e. kinds)
# Redmine trackers can be found at <redmineUrl>/trackers
# Possible Bitbucket kinds:
# "bug"
# "enhancement"
# "proposal"
# "task"
TRACKER_TO_KIND_MAP = {
    'Design': 'task',
    'Bug': 'bug',
    'Feature': 'enhancement',
    'Vendor': 'task',
    'Data': 'task',
    'Support': 'task',
}
# Map statuses from Redmine to Bitbucket
# Redmine statuses can be found at <redmineUrl>/issue_statuses
# Possible BitBucket statuses:
# "new"
# "open"
# "resolved"
# "on hold"
# "invalid"
# "duplicate"
# "wontfix"
STATUS_MAP = {
    'Resolved': 'resolved',
    'Closed': 'resolved',
    'New': 'new',
    'Assigned': 'open',
    'Feedback': 'on hold',
    'On Hold': 'on hold',
    'Rejected': 'invalid',
}
# Map priorities from Redmine to Bitbucket.
# Shouldn't need to change this since Redmine priorites are immutable.
# Possible BitBucket priorities:
# "trivial"
# "minor"
# "major"
# "critical"
# "blocker"
PRIORITY_MAP = {
    'Low': 'trivial',
    'Normal': 'minor',
    'High': 'major',
    'Urgent': 'critical',
    'Immediate': 'blocker',
}
# Directory to output the ZIP files (will be created if it doesn't exist)
OUTPUT_DIR = os.path.join(BASE_DIR, 'issues')
# Directory containing ZIP exports for existing issues (will be ignored if it doesn't exist)
INPUT_DIR = os.path.join(BASE_DIR, 'issues_old')
# Version of Redmine
REDMINE_VERSION = 3.2


TMP_DIR = os.path.join(BASE_DIR, 'tmp')

EXPORT_ATTACHMENTS = True

MARK_UNMAPPED_USERS_AS_ANONYMOUS = False

SAVE_ADDITIONAL_FIELDS = False

TEXT_CONVERSION = ('textile', 'markdown_github')

# Add all assignee and comment authors to issue watchers
ADD_WATCHERS = True

try:
    from local_settings import *
except ImportError:
    pass
