#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013 The American Society for Clinical Investigation
#
# Small script that retrieves issues from Redmine's REST API and generates zip
# files that can be imported into Bitbucket's issue tracker using the issue importer.
# Depends on pyredminews: https://github.com/ianepperson/pyredminews
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import os
import sys
import redmine
from feeds import RedmineIssueFeed
from settings import OUTPUT_DIR, REDMINE_VERSION, TMP_DIR
import shutil

if __name__ == '__main__':
    if len(sys.argv) != 4:
        usage = "Usage: {0} <redmineUrl> <redmineApiKey> <projectId>\nExample: {0} http://redmine.jci.org f00ba5 test"
        print(usage.format(sys.argv[0]))
        sys.exit(1)
    redmine_url = sys.argv[1]
    redmine_api_key = sys.argv[2]
    redmine_project = sys.argv[3]

    for d in [OUTPUT_DIR, TMP_DIR]:
        if os.path.isdir(d):
            shutil.rmtree(d)
    os.mkdir(OUTPUT_DIR)
    os.mkdir(TMP_DIR)

    redmine = redmine.Redmine(redmine_url, key=redmine_api_key, debug=False, version=REDMINE_VERSION)
    feed = RedmineIssueFeed(redmine)
    for issue in redmine.issue.filter(project_id=redmine_project, status_id='*'):
        feed.add_issue(issue)
    feed.save_issues()
